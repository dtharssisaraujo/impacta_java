/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.impacta.ads.lp3.empresa.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author danilodeveloper
 */

@Entity
@Table(name = "DEPARTAMENTO", schema = "EMPRESA")

public class Departamento implements Serializable {

    @Id
    private Integer codigo;
    private String nome;

    public Departamento() {
    }

    public Departamento(Integer codigo) {
        this.codigo = codigo;
    }

    public Departamento(Integer codigo, String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
