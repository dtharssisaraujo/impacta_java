/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.impacta.ads.lp3.empresa.dao.jdbc;

import br.edu.impacta.ads.lp3.empresa.model.Departamento;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Israel
 */
public interface GenericoDAO<E> extends Serializable {

    void inserir(E e);

    void remover(E e);

    void alterar(E e);

    List<E> listar();
}
