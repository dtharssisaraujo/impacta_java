/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.impacta.ads.lp3.empresa.dao.jpa;

import br.edu.impacta.ads.lp3.empresa.dao.jdbc.GenericoDAO;
import br.edu.impacta.ads.lp3.empresa.model.Funcionario;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author danilodeveloper
 */
public class FuncionarioDAO implements GenericoDAO<Funcionario> {

    @Override
    public void inserir(Funcionario funcionario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remover(Funcionario funcionario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void alterar(Funcionario funcionario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
                                                                                 
    @Override
    public List<Funcionario> listar() {
        List<Funcionario> funcionarios = new ArrayList<>();
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("EmpresaAppPU");                                                                                                                      
        EntityManager entity = emf.createEntityManager();
        Query query = entity.createQuery("SELECT c FROM Funcionario c");
        funcionarios = query.getResultList();
        emf.close();
        return funcionarios;
    }
    
    public static void main(String[] args) {
        for(Funcionario func : new FuncionarioDAO().listar()) {
            System.out.println(func.getNome() + "\n\t" + func.getCargo().getNome());
        }
    }
}
