/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.impacta.ads.lp3.empresa.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author danilodeveloper
 */

@Entity
@Table(name = "FUNCIONARIO", schema = "EMPRESA")

public class Funcionario implements Serializable {

    @Id
    @Column(name = "ID_FUNCIONARIO")
    private Integer codigo;
    private String nome;
    @Column (name = "DT_CONTRATACAO")
    @Temporal(TemporalType.DATE)
    private Date contratacao;
    
    @ManyToOne (optional = false)
    @JoinColumn ( name = "ID_CARGO", referencedColumnName = "ID_CARGO")
    private Cargo cargo;
    
    @ManyToOne (optional = false)
    @JoinColumn ( name = "ID_DEPARTAMENTO", referencedColumnName = "ID_DEPARTAMENTO")
    private Departamento departamento;

    public Funcionario() {
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getContratacao() {
        return contratacao;
    }

    public void setContratacao(Date contratacao) {
        this.contratacao = contratacao;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

}
